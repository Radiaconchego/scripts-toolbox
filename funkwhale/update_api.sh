set -ex

FUNKWHALE_VERSION="1.2.8"

cd /srv/funkwhale

# download more recent API files
sudo -u funkwhale curl -L -o "api-$FUNKWHALE_VERSION.zip" "https://dev.funkwhale.audio/funkwhale/funkwhale/-/jobs/artifacts/$FUNKWHALE_VERSION/download?job=build_api"

sudo -u funkwhale unzip "api-$FUNKWHALE_VERSION.zip" -d extracted
sudo -u funkwhale rm -rf api/ && sudo -u funkwhale mv extracted/api .
sudo -u funkwhale rm -rf extracted
sudo -u funkwhale rm api-$FUNKWHALE_VERSION.zip

# update os dependencies
sudo api/install_os_dependencies.sh install
sudo -u funkwhale -H -E /srv/funkwhale/virtualenv/bin/pip install -r api/requirements.txt

# collect static files
sudo -u funkwhale -H -E /srv/funkwhale/virtualenv/bin/python api/manage.py collectstatic --no-input

# stop the services
sudo systemctl stop funkwhale.target

# apply database migrations
sudo -u funkwhale -H -E /srv/funkwhale/virtualenv/bin/python api/manage.py migrate

# restart the services
sudo systemctl start funkwhale.target
