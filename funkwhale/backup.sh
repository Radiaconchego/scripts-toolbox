#!/bin/bash
# sudo apt -y install p7zip-full
# Save this file at /opt/funkwhale-backup/backup.sh
# mkdir -p /opt/funkwhale-backup/
# mkdir -p /opt/funkwhale-backup/logs
# Example cron job:
# 0 3 * * * bash /opt/funkwhale-backup/backup.sh > /opt/funkwhale-backup/logs/backup.log 2>&1
set -exu

umask 027

# Replace the values below with your Rclone config
#rclone_config_name="rclone_config_name"
#rclone_config_pass="yourpassword"
#s3_bucket_name="bucket_name"


BACKUP_BASE_DIR="$HOME/funkwhale/backup"

FUNKWHALE_BASE_DIR="/srv/funkwhale"

BACKUP_TIME="$(date +"%d_%m_%Y_%H_%M_%S")"
BACKUP_FILENAME=${BACKUP_TIME}_`echo $RANDOM`

BACKUP_DIR=${BACKUP_BASE_DIR}/${BACKUP_FILENAME}
BACKUP_DATABASE_DIR=$BACKUP_DIR/database
BACKUP_CONFIG_DIR=$BACKUP_DIR/instance_configuration
BACKUP_DATA_DIR=$BACKUP_DIR/instance_data
BACKUP_NGINX_DIR=$BACKUP_DIR/nginx_configuration

# Remove old backup directory
rm -rf $BACKUP_BASE_DIR

# Create temporary backup directories
mkdir -p $BACKUP_BASE_DIR
# [ -e ${BACKUP_DIR} ] || mkdir -p ${BACKUP_DIR}
mkdir -p $BACKUP_DATABASE_DIR
mkdir -p $BACKUP_CONFIG_DIR
mkdir -p $BACKUP_DATA_DIR
mkdir -p $BACKUP_NGINX_DIR

# Generate a database dump backup
sudo -u postgres -H pg_dump funkwhale > $BACKUP_DATABASE_DIR/backup_dump_${BACKUP_TIME}.sql

# Copy instance config
rsync -avzhP $FUNKWHALE_BASE_DIR/config/.env $BACKUP_CONFIG_DIR/backup.env

# Copy instance data
rsync -avzhP $FUNKWHALE_BASE_DIR/data/media $BACKUP_DATA_DIR/media
rsync -avzhP $FUNKWHALE_BASE_DIR/data/music $BACKUP_DATA_DIR/music

# Copy Nginx config
cp /etc/nginx/sites-available/* $BACKUP_NGINX_DIR/

rclone sync $BACKUP_DIR wasabi:sonora-bkp/$backup_file_name -P --create-empty-src-dirs

# Compress
# 7z a $FUNKWHALE_BASE_DIR/backup/${backup_file_name}.7z $BACKUP_DIR

# Load the rclone password
#export RCLONE_CONFIG_PASS=$rclone_config_pass

# Upload the backup
# rclone move $FUNKWHALE_BASE_DIR/backup/${backup_file_name}.7z $rclone_config_name:$s3_bucket_name/

# Remove backup directory
# rm -rf $FUNKWHALE_BASE_DIR/backup/*
