set -exu

export FUNKWHALE_VERSION="1.2.8"
cd /srv/funkwhale
sudo -u funkwhale curl -L -o front.zip "https://dev.funkwhale.audio/funkwhale/funkwhale/builds/artifacts/$FUNKWHALE_VERSION/download?job=build_front"
sudo -u funkwhale unzip -o front.zip
sudo -u funkwhale rm front.zip
